const ObjectsToCsv = require('objects-to-csv');

const scraperObject = {
    url: 'https://hubsch-interior.com/fr/produits/',
    async scraper(browser) {
        let page = await browser.newPage();
        console.log(`Navigating to ${this.url}...`);
        // Navigate to the selected page
        await page.goto(this.url);
        // Wait for the required DOM to be rendered
        async function scrapeCurrentPage(){
            // Get the link to all the products
            var result = await page.evaluate(() => {
                const links = Array.from(document.querySelectorAll('.swiper-wrapper > a.swiper-slide:nth-child(1)'));
                var link = links.map((link) => link.href);
                return link;
            })

            //Loop through each of those links, open a new page instance and get the relevant data from them
            let pagePromise = (link) => new Promise(async(resolve) => {
                let dataObj = {};
                let newPage = await browser.newPage();
                await newPage.goto(link);
                dataObj['product_title'] = await newPage.$eval('.elementor-widget-woocommerce-product-title', text => text.textContent.replace(/(\r\n\t|\n|\r|\t)/gm, ""));
                dataObj['retail-price'] = await newPage.$eval('.retail-price', text => text.textContent.replace(/(\r\n\t|\n|\r|\t)/gm, ""));
                dataObj['product_ref'] = await newPage.$eval('.woocommerce-product-attributes-item__value', text => text.textContent.replace(/(\r\n\t|\n|\r|\t)/gm, ""));
                resolve(dataObj); // si la promesse est tenue
                await newPage.close();
            })
            for(link in result){
                let currentPageData = await pagePromise(result[link]); //result[link] est la valeur de résolution si la promesse plus haut est tenue

                //csv file
                let datas = [currentPageData];
                (async () => {
                    const csv = new ObjectsToCsv(datas);
                   
                    // Save to file:
                    await csv.toDisk('./products.csv', { append: true });
                   
                    // Return the CSV file as string:
                    console.log(await csv.toString());
                })();
            }
            // When all the data on this page is done, click the next button and start the scraping of the next page
            // You are going to check if this button exist first, so you know if there really is a next page.
            let nextButtonExist = false;
            try{
                const nextButton = await page.$eval('.next > a', a => a.textContent);
                nextButtonExist = true;
            }
            catch(err){
                nextButtonExist = false;
            }
            if(nextButtonExist){
                await page.click('.next > a');   
                return scrapeCurrentPage(); // Call this function recursively
            }
            await page.close();
        }
        await scrapeCurrentPage();
    }
}

module.exports = scraperObject;